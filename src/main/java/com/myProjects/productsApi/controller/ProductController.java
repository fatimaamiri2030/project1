package com.myProjects.productsApi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myProjects.productsApi.model.ProductEntity;
import com.myProjects.productsApi.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/products")
	public ResponseEntity<List<ProductEntity>>  getProducts(){
		return new ResponseEntity<>(productService.getProducts() , HttpStatus.OK) ;  
	}
	
	@GetMapping("/products/{id}")
	public ResponseEntity<Optional<ProductEntity>> getProduct(@PathVariable("id") final int id) {
		return new ResponseEntity<>(productService.getProduct(id) , HttpStatus.OK) ;  
	}
	
	@PostMapping("/products")
	public ResponseEntity<ProductEntity> saveProduct(@RequestBody ProductEntity productEntity) {
		return new ResponseEntity<>(productService.saveProduct(productEntity) , HttpStatus.CREATED ) ; 
	}
	
	@PutMapping("/products/{id}")
	public ResponseEntity<ProductEntity> updateProduct(@RequestBody ProductEntity productEntity , @PathVariable("id") final int id) {
		return new ResponseEntity<>(productService.updateProduct(productEntity, id) ,HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping("/products/{id}")
	public void deleteProduct(@PathVariable("id") final int id) {
		productService.deleteProduct(id);
	}
}

package com.myProjects.productsApi.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myProjects.productsApi.exception.ProductNotFound;
import com.myProjects.productsApi.model.ProductEntity;
import com.myProjects.productsApi.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public List<ProductEntity> getProducts(){
		return productRepository.findAll();
	}
	
	public Optional<ProductEntity> getProduct(final int id) {
		Optional<ProductEntity> optional=productRepository.findById(id);
		if(optional.isPresent())
			return optional;
		else
		  	throw new ProductNotFound("Not found product with id = "+id);
	}  
	
	public ProductEntity saveProduct(ProductEntity productEntity) {
		return productRepository.save(productEntity); 
	}
	
	public ProductEntity updateProduct(ProductEntity productEntity , final int id) {
		Optional<ProductEntity> optional=productRepository.findById(id);
		if(optional.isPresent()) {
			productEntity.setId(id);
			return productRepository.save(productEntity);
		}		
		else
			throw new ProductNotFound("Not found product with id = "+id);	
	}
	
	public void deleteProduct(final int id) {
		Optional<ProductEntity> optional=productRepository.findById(id);
		if(optional.isPresent())
			productRepository.deleteById(id);
		else
			throw new ProductNotFound("Not found product with id = "+id);	
	} 

}

package com.myProjects.productsApi.message;

import java.util.Date;

public class ErrorMessage {
	
	private String message;
	private Date timestamp;
	private Integer code;
	private String description;
	
	public ErrorMessage(String message, Date timestamp, Integer code, String description) {
		super();
		this.message = message;
		this.timestamp = timestamp; 
		this.code = code;
		this.description = description;
	}

	public ErrorMessage() {
		super();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
	

}
